# _Sentence Scrambler_

### _Takes user input and scrambles the sentence, August 15, 2016_

#### _**By Aimen Khakwani and Ethan Law**_

## Description

_A webpage that scrambles your sentence._

##Setup and Installation

* _Clone_
* _Run in browser_

## Technologies Used

_HTML, CSS, Bootstrap, jQuery, and JavaScript_

### License
Copyright (c) 2016 **_Aimen Khakwani & Ethan Law_**
